﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Day07MvcAuth.Startup))]
namespace Day07MvcAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
