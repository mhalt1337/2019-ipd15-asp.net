﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Class8Example1.Controllers
{
    public class QuebecController : Controller
    {
        // GET: Quebec
        public ActionResult Index()
        {
            return View();
        }

        public string Montreal(string name)
        {
            return String.Format("Hello Montreal from {0}", name);
        }
    }
}