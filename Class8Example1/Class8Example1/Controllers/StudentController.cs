﻿using Class8Example1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Class8Example1.Controllers
{
    public class StudentController : Controller
    {

        public static IList<Student> studentList = new List<Student>
        {
            new Student() { StudentId=1, StudentName="Maia", StudentAge=18},
            new Student() { StudentId=2, StudentName="Heidi", StudentAge=18},
            new Student() { StudentId=3, StudentName="Johann", StudentAge=18}
        };

        // GET: Student
        public ActionResult Index()
        {
            return View(studentList);
        }

        public ActionResult Edit(int id)
        {
            var student = studentList.Where(s => s.StudentId == id).FirstOrDefault();
            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(Student student)
        {
            return RedirectToAction("Index");

        }
    }
}