1. Create a new ASP.NET WebForm application
2. Add a new Login.aspx page to the project
3. On the Login page the user can enter his Username and Password and Submit the info
4. Add RequiredField Validators for both Username and Password
5. Create a Login link on the top menu next to Contact link from where the user can access the Login page
6. The Application will have 2 states: User logged in and User logged off
7. When the User is logged in, the Login link will be replaced with a Logoff link
8. When the User is logged off, clicking on any of the menu pages he/she will be redirected to the same Login.aspx page
9. When the User clicks on the Logoff link the User will be redirected to the Login.aspx page and the Logoff link will be replaced with the Login link on the top menu