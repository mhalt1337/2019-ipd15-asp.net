﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageStudents.aspx.cs" Inherits="Students.ManageStudents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id,Id1" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                    <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" SortExpression="PhoneNumber" />
                    <asp:BoundField DataField="AddressId" HeaderText="AddressId" SortExpression="AddressId" />
                    <asp:BoundField DataField="ClassId" HeaderText="ClassId" SortExpression="ClassId" />
                    <asp:BoundField DataField="Id1" HeaderText="Id1" InsertVisible="False" ReadOnly="True" SortExpression="Id1" />
                    <asp:BoundField DataField="Street" HeaderText="Street" SortExpression="Street" />
                    <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                    <asp:BoundField DataField="Province" HeaderText="Province" SortExpression="Province" />
                    <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                    <asp:BoundField DataField="Id2" HeaderText="Id2" InsertVisible="False" ReadOnly="True" SortExpression="Id2" />
                    <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="Number" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:StudentsConnectionString2 %>" SelectCommand="SELECT Student.*, Address.*, Class.* FROM Address INNER JOIN Class ON Address.Id = Class.Id INNER JOIN Student ON Address.Id = Student.Id"></asp:SqlDataSource>
            <br />
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="Id,Id1" DataSourceID="SqlDataSource1">
                <AlternatingItemTemplate>
                    <tr style="background-color: #FFFFFF;color: #284775;">
                        <td>
                            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
                        </td>
                        <td>
                            <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("FirstName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("LastName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                        </td>
                        <td>
                            <asp:Label ID="AddressIdLabel" runat="server" Text='<%# Eval("AddressId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ClassIdLabel" runat="server" Text='<%# Eval("ClassId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id1Label" runat="server" Text='<%# Eval("Id1") %>' />
                        </td>
                        <td>
                            <asp:Label ID="StreetLabel" runat="server" Text='<%# Eval("Street") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CityLabel" runat="server" Text='<%# Eval("City") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ProvinceLabel" runat="server" Text='<%# Eval("Province") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PostalCodeLabel" runat="server" Text='<%# Eval("PostalCode") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id2Label" runat="server" Text='<%# Eval("Id2") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NumberLabel" runat="server" Text='<%# Eval("Number") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                    <tr style="background-color: #999999;">
                        <td>
                            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                        </td>
                        <td>
                            <asp:Label ID="IdLabel1" runat="server" Text='<%# Eval("Id") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="AddressIdTextBox" runat="server" Text='<%# Bind("AddressId") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ClassIdTextBox" runat="server" Text='<%# Bind("ClassId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id1Label1" runat="server" Text='<%# Eval("Id1") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ProvinceTextBox" runat="server" Text='<%# Bind("Province") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="PostalCodeTextBox" runat="server" Text='<%# Bind("PostalCode") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id2Label1" runat="server" Text='<%# Eval("Id2") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="NumberTextBox" runat="server" Text='<%# Bind("Number") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                        </td>
                    </tr>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <table runat="server" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                        <tr>
                            <td>No data was returned.</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="AddressIdTextBox" runat="server" Text='<%# Bind("AddressId") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ClassIdTextBox" runat="server" Text='<%# Bind("ClassId") %>' />
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ProvinceTextBox" runat="server" Text='<%# Bind("Province") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="PostalCodeTextBox" runat="server" Text='<%# Bind("PostalCode") %>' />
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="NumberTextBox" runat="server" Text='<%# Bind("Number") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                        </td>
                    </tr>
                </InsertItemTemplate>
                <ItemTemplate>
                    <tr style="background-color: #E0FFFF;color: #333333;">
                        <td>
                            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
                        </td>
                        <td>
                            <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("FirstName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("LastName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                        </td>
                        <td>
                            <asp:Label ID="AddressIdLabel" runat="server" Text='<%# Eval("AddressId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ClassIdLabel" runat="server" Text='<%# Eval("ClassId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id1Label" runat="server" Text='<%# Eval("Id1") %>' />
                        </td>
                        <td>
                            <asp:Label ID="StreetLabel" runat="server" Text='<%# Eval("Street") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CityLabel" runat="server" Text='<%# Eval("City") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ProvinceLabel" runat="server" Text='<%# Eval("Province") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PostalCodeLabel" runat="server" Text='<%# Eval("PostalCode") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id2Label" runat="server" Text='<%# Eval("Id2") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NumberLabel" runat="server" Text='<%# Eval("Number") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table runat="server">
                        <tr runat="server">
                            <td runat="server">
                                <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                                    <tr runat="server" style="background-color: #E0FFFF;color: #333333;">
                                        <th runat="server">Id</th>
                                        <th runat="server">FirstName</th>
                                        <th runat="server">LastName</th>
                                        <th runat="server">PhoneNumber</th>
                                        <th runat="server">AddressId</th>
                                        <th runat="server">ClassId</th>
                                        <th runat="server">Id1</th>
                                        <th runat="server">Street</th>
                                        <th runat="server">City</th>
                                        <th runat="server">Province</th>
                                        <th runat="server">PostalCode</th>
                                        <th runat="server">Id2</th>
                                        <th runat="server">Number</th>
                                        <th runat="server">Name</th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server" style="text-align: center;background-color: #5D7B9D;font-family: Verdana, Arial, Helvetica, sans-serif;color: #FFFFFF"></td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <SelectedItemTemplate>
                    <tr style="background-color: #E2DED6;font-weight: bold;color: #333333;">
                        <td>
                            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
                        </td>
                        <td>
                            <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("FirstName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("LastName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                        </td>
                        <td>
                            <asp:Label ID="AddressIdLabel" runat="server" Text='<%# Eval("AddressId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ClassIdLabel" runat="server" Text='<%# Eval("ClassId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id1Label" runat="server" Text='<%# Eval("Id1") %>' />
                        </td>
                        <td>
                            <asp:Label ID="StreetLabel" runat="server" Text='<%# Eval("Street") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CityLabel" runat="server" Text='<%# Eval("City") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ProvinceLabel" runat="server" Text='<%# Eval("Province") %>' />
                        </td>
                        <td>
                            <asp:Label ID="PostalCodeLabel" runat="server" Text='<%# Eval("PostalCode") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Id2Label" runat="server" Text='<%# Eval("Id2") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NumberLabel" runat="server" Text='<%# Eval("Number") %>' />
                        </td>
                        <td>
                            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                        </td>
                    </tr>
                </SelectedItemTemplate>
            </asp:ListView>
            <br />
            <asp:FormView ID="FormView1" runat="server" DataKeyNames="Id,Id1" DataSourceID="SqlDataSource1">
                <EditItemTemplate>
                    Id:
                    <asp:Label ID="IdLabel1" runat="server" Text='<%# Eval("Id") %>' />
                    <br />
                    FirstName:
                    <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                    <br />
                    LastName:
                    <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                    <br />
                    PhoneNumber:
                    <asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' />
                    <br />
                    AddressId:
                    <asp:TextBox ID="AddressIdTextBox" runat="server" Text='<%# Bind("AddressId") %>' />
                    <br />
                    ClassId:
                    <asp:TextBox ID="ClassIdTextBox" runat="server" Text='<%# Bind("ClassId") %>' />
                    <br />
                    Id1:
                    <asp:Label ID="Id1Label1" runat="server" Text='<%# Eval("Id1") %>' />
                    <br />
                    Street:
                    <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>' />
                    <br />
                    City:
                    <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                    <br />
                    Province:
                    <asp:TextBox ID="ProvinceTextBox" runat="server" Text='<%# Bind("Province") %>' />
                    <br />
                    PostalCode:
                    <asp:TextBox ID="PostalCodeTextBox" runat="server" Text='<%# Bind("PostalCode") %>' />
                    <br />
                    Id2:
                    <asp:Label ID="Id2Label1" runat="server" Text='<%# Eval("Id2") %>' />
                    <br />
                    Number:
                    <asp:TextBox ID="NumberTextBox" runat="server" Text='<%# Bind("Number") %>' />
                    <br />
                    Name:
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    FirstName:
                    <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                    <br />
                    LastName:
                    <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                    <br />
                    PhoneNumber:
                    <asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' />
                    <br />
                    AddressId:
                    <asp:TextBox ID="AddressIdTextBox" runat="server" Text='<%# Bind("AddressId") %>' />
                    <br />
                    ClassId:
                    <asp:TextBox ID="ClassIdTextBox" runat="server" Text='<%# Bind("ClassId") %>' />
                    <br />

                    Street:
                    <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>' />
                    <br />
                    City:
                    <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                    <br />
                    Province:
                    <asp:TextBox ID="ProvinceTextBox" runat="server" Text='<%# Bind("Province") %>' />
                    <br />
                    PostalCode:
                    <asp:TextBox ID="PostalCodeTextBox" runat="server" Text='<%# Bind("PostalCode") %>' />
                    <br />

                    Number:
                    <asp:TextBox ID="NumberTextBox" runat="server" Text='<%# Bind("Number") %>' />
                    <br />
                    Name:
                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Id:
                    <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
                    <br />
                    FirstName:
                    <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' />
                    <br />
                    LastName:
                    <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' />
                    <br />
                    PhoneNumber:
                    <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Bind("PhoneNumber") %>' />
                    <br />
                    AddressId:
                    <asp:Label ID="AddressIdLabel" runat="server" Text='<%# Bind("AddressId") %>' />
                    <br />
                    ClassId:
                    <asp:Label ID="ClassIdLabel" runat="server" Text='<%# Bind("ClassId") %>' />
                    <br />
                    Id1:
                    <asp:Label ID="Id1Label" runat="server" Text='<%# Eval("Id1") %>' />
                    <br />
                    Street:
                    <asp:Label ID="StreetLabel" runat="server" Text='<%# Bind("Street") %>' />
                    <br />
                    City:
                    <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>' />
                    <br />
                    Province:
                    <asp:Label ID="ProvinceLabel" runat="server" Text='<%# Bind("Province") %>' />
                    <br />
                    PostalCode:
                    <asp:Label ID="PostalCodeLabel" runat="server" Text='<%# Bind("PostalCode") %>' />
                    <br />
                    Id2:
                    <asp:Label ID="Id2Label" runat="server" Text='<%# Eval("Id2") %>' />
                    <br />
                    Number:
                    <asp:Label ID="NumberLabel" runat="server" Text='<%# Bind("Number") %>' />
                    <br />
                    Name:
                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                    <br />

                </ItemTemplate>
            </asp:FormView>
        </div>
    </form>
</body>
</html>
