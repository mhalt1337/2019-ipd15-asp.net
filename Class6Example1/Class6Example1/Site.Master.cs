﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Class6Example1
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!String.IsNullOrEmpty(Session["Username"].ToString()))
                {
                    LinkButton3.Text = "Logoff";
                }
                else
                {
                    LinkButton3.Text = "Login";
                }
            }
            else
                LinkButton3.Text = "Login";
        }


        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!String.IsNullOrEmpty(Session["Username"].ToString()))
                {

                    Response.Redirect("About.aspx");
                }
                else
                {

                    Response.Redirect("Login.aspx");
                }
            }
            else
                Response.Redirect("Login.aspx");
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!String.IsNullOrEmpty(Session["Username"].ToString()))
                {

                    Response.Redirect("Contact.aspx");
                }
                else
                {

                    Response.Redirect("Login.aspx");
                }
            }
            else
                Response.Redirect("Login.aspx");
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!String.IsNullOrEmpty(Session["Username"].ToString()))
                {

                    Session.Abandon();
                    Response.Redirect("Default.aspx");
                }
                else
                {

                    Response.Redirect("Login.aspx");
                }
            }
            else
                Response.Redirect("Login.aspx");
        }
    }
}