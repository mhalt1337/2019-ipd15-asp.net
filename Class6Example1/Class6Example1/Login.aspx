﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Class6Example1.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /><br />
    Username: 
    <asp:TextBox ID="Username" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Username" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
    <br /><br />
    Password: 
    <asp:TextBox ID="Password" TextMode="Password" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Password" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
    <br /><br />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="Red" ErrorMessage="Please type the correct credentials" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
    <br /><br />
    <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
</asp:Content>
