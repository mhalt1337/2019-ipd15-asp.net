﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Class6Example1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string teacherName = System.Configuration.ConfigurationManager.AppSettings["TeacherName"];
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //create the user session state
                Session["Username"] = Global.username;
                Session["Password"] = Global.password;

                Response.Redirect("Default.aspx");
            }
            else
            {
                CustomValidator1.ErrorMessage = String.Format("You dude have a problem with the value: {0}", Username.Text);
            }

        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //check text boxes values againsts the Global values
            if(Username.Text.Equals(Global.username) && Password.Text.Equals(Global.password))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }

        }
    }
}