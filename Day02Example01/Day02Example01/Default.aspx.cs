﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Day02Example01
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            lblFName.Text = "Hello from pageInit";
        }

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            lblFName.Text = "Hello from pageInitComplete";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblFName.Text = "Hello from pageLoad";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //lblFName.Text = sender.ToString(); ***System.Web.UI.WebControls.Button***
            //lblFName.Text = this.ToString(); ***ASP.default_aspx***
            if (String.IsNullOrEmpty(tbFname.Text))
            {
                //mpePopUp.Show();
                
            } else
            {
                lblFName.Text = "Hello " + tbFname.Text;
            }
        }

        protected void greetList_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblFName.Text = greetList.SelectedValue;
        }
    }
}