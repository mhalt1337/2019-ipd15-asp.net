﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Day02Example01.Default" Trace="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <%//this.lblFName.Text = tbFname.Text;    %>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManger1" runat="Server">
        </asp:ScriptManager>
        <div>
            <asp:Label Text="Hello World!" ID="lblFName" runat="server" for="fName" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator">
                <asp:TextBox ID="tbFname" runat="server"></asp:TextBox>
            </asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Click Me" OnClick="Button1_Click" />
            <asp:DropDownList ID="greetList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="greetList_SelectedIndexChanged">
                <asp:ListItem Value="No One"></asp:ListItem>
                <asp:ListItem Value="World"></asp:ListItem>
                <asp:ListItem Value="Universe"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:Label ID="lblHidden" runat="server" Text=""></asp:Label>
        <ajaxToolkit:ModalPopupExtender ID="mpePopUp" runat="server" TargetControlID="lblHidden" PopupControlID="divPopUp" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

        <div id="divPopUp" class="pnlBackGround modal">
            <div id="Header" class="header modal-header">Error:</div>
            <div id="main" class="main modal-body">Values cannot be empty! </div>
            <div id="buttons">
                <div id="DivbtnOK" class="buttonOK">
                    <asp:Button ID="btnOk" class="btn btn-primary" runat="server" Text="Ok" />
                </div>
            </div>
        </div>
    </form>

    <%--    <div id="modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>--%>
</body>
</html>
