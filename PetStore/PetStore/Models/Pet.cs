﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    //Breed(enum with the values: Dog, Cat, Fish, Hamster, Guinea Pig)
    public enum Breed
    {
        Dog,
        Cat,
        Fish,
        Hamster,
        [Display(Name = "Guinea Pig")]
        GuineaPig
    }

    public class Pet
    {
        public int PetId { get; set; }
        public int OwnerId { get; set; }
        [Required, StringLength(maximumLength: 10)]
        public string Name { get; set; }
        [Required, DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        public Breed? Breed { get; set; }

        public virtual Owner Owner { get; set; }

    }
}
