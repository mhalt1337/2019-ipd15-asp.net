﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class Owner
    {
        public int OwnerId { get; set; }
        [Required, StringLength(maximumLength: 20)]
        public string FullName { get; set; }
        [Required, StringLength(maximumLength: 20)]
        public string Street { get; set; }
        [Required, StringLength(maximumLength: 20)]
        public string City { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Required, DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required, StringLength(maximumLength: 20)]
        public string Occupation { get; set; }

        //The Pets property is a navigation property.
        //Navigation properties hold other entities that are related to this entity.
        public virtual ICollection<Pet> Pets { get; set; }
    }
}
