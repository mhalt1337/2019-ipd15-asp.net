﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaiaStore.Models
{
    public class Cart
    {
        private List<OrderDetail> selections = new List<OrderDetail>();

        public Cart AddItem(Product p, int quantity)
        {
            OrderDetail detail = selections
                .Where(l => l.ProductId == p.Id).FirstOrDefault();
            if (detail != null)
            {
                detail.Quantity += quantity;
            }
            else
            {
                selections.Add(new OrderDetail
                {
                    ProductId = p.Id,
                    Product = p,
                    Quantity = quantity
                });
            }
            return this;
        }

        public Cart RemoveItem(long productId)
        {
            selections.RemoveAll(l => l.ProductId == productId);
            return this;
        }

        public void Clear() => selections.Clear();

        public IEnumerable<OrderDetail> Selections { get => selections; }
    }
}