﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Class5Example1
{
    public partial class SessionObject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String cs = Global.ConnectionString;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TextBox1.Text))
            {
                // Access the HttpServerUtility methods through
                // the intrinsic Server object.
                Label1.Text = "Welcome, " + 
                    Server.HtmlEncode(TextBox1.Text) +
                    ".<br/> The url is " +
                    Server.UrlEncode(Request.Url.ToString()) +
                    "<br/> The machine name is " + Server.MachineName +
                    "<br/> Session ID: " + Session.SessionID;
            }
        }
    }
}