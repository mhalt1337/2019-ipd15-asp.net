﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Class5Example1
{
    public class Global : System.Web.HttpApplication
    {
        protected DateTime beginRequestTime;
        public static readonly string ConnectionString = "connection information";

        protected void Application_Start(object sender, EventArgs e)
        {
            Application["ConnectionString2"] = "connection information 2";
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            beginRequestTime = DateTime.Now;
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            string messageFormat = "Elapsed request time (ms) = {0}";
            TimeSpan diffTime = DateTime.Now - beginRequestTime;
            Trace.WriteLine(String.Format(messageFormat, diffTime.TotalMilliseconds));

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}