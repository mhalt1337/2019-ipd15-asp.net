﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Day09ContosoUniversity.Models
{
    public class Student
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public string Secret { get; set; }

        //The Enrollments property is a navigation property.
        //Navigation properties hold other entities that are related to this entity.
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}