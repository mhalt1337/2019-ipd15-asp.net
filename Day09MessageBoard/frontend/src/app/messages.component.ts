import { Component } from "@angular/core"

@Component({
  selector: 'messages',
  template: 'this is the messages component <div *ngFor="let message of messages">{{message.text}} by: {{message.owner}}</div>'
})

export class MessagesComponent {
  messages = [
    { text: 'some text from MAIA', owner: 'Maia' },
    { text: 'some text from HEIDI', owner: 'Heidi' },
    { text: 'some text from JOHANN', owner: 'Johann' }
  ];
}
