﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVCCoreMovie.Models
{
    public class MVCCoreMovieContext : DbContext
    {
        public MVCCoreMovieContext (DbContextOptions<MVCCoreMovieContext> options)
            : base(options)
        {
        }

        public DbSet<MVCCoreMovie.Models.Movie> Movie { get; set; }
    }
}
