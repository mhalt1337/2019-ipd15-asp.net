﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class ServerObject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TextBox1.Text))
            {
                // Access the HttpServerUtility methods through
                // the intrinsic Server object.
                Label1.Text = "Welcome, " +
                Server.HtmlEncode(TextBox1.Text) +
                ".<br/> The url is " +
                Server.UrlEncode(Request.Url.ToString());
            }
        }
    }
}