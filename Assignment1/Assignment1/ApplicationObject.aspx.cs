﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class ApplicationObject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string connectionString = Application["ConnectionString"].ToString();
            string connectionString = Global.ConnectionString;

            //DataSet cachedData = (DataSet)Application[“MyDataSet”];
            //string myString = Application[“MyString”].ToString();
            DataSet cachedData = Global.MyDataSet;
            string  myString = Global.MyString;

            Label1.Text = connectionString;
            Label2.Text = myString;
        }
    }
}