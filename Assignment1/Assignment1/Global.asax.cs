﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Assignment1
{
    public class Global : System.Web.HttpApplication
    {
        public static readonly string ConnectionString = "connection information";
        internal static readonly DataSet MyDataSet;
        internal static readonly string MyString;
        protected DateTime beginRequestTime;

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            beginRequestTime = DateTime.Now;
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            string messageFormat = "Elapsed request time (ms) = {0}";
            TimeSpan diffTime = DateTime.Now - beginRequestTime;
            Trace.WriteLine(String.Format(messageFormat,
            diffTime.TotalMilliseconds));
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}