﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServerObject.aspx.cs" Inherits="Assignment1.ServerObject" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HttpServerUtility Example</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Enter your name:<br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click"
                Text="Submit" />
            <br />
            <asp:Label ID="Label1" runat="server" />
        </div>
    </form>
</body>
</html>
