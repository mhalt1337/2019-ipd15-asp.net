﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProjectEcommerce.Models
{
    public enum Category
    {
        Electronics, Sports, Music, Books
    }


    public class Product
    {
        public int ProductId { get; set; }
        public int SupplierId { get; set; }
        public Category Category { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [Range(0.01, 100.00, ErrorMessage = "Price must be between 0.01 and 100.00")]
        public decimal Price { get; set; }
        [StringLength(1024)]
        public string ImageURL { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }




    }
}
